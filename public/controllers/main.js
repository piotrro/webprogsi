// GET announcement page
module.exports.announcement = function(req, res, next) {
  res.render('announcement', { title: 'Express' });
}

module.exports.crud = function(req, res, next) {
  res.render('crud', { title: 'Express' });
}

module.exports.index = function(req, res, next) {
  res.render('index', { title: 'Express' });
}

module.exports.login = function(req, res, next) {
  res.render('login', { title: 'Express' });
}

module.exports.phone_category = function(req, res, next) {
  res.render('phone_category', { title: 'Express' });
}

module.exports.phone_search = function(req, res, next) {
  res.render('phone_search', { title: 'Express' });
}

module.exports.register = function(req, res, next) {
  res.render('register', { title: 'Express' });
}

module.exports.user = function(req, res, next) {
  res.render('user', { title: 'Express' });
}

