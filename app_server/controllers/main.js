// GET announcement page
var url = require('url');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Ann = mongoose.model('announcement');
var Category = mongoose.model('category');

module.exports.announcement = function(req, res, next) {
  res.render('announcement', { title: 'Express' });
}

module.exports.crud = function(req, res, next) {
  res.render('crud', { title: 'Express' });
}

module.exports.index = function(req, res, next) {
	res.render('index', { title: 'Express' });
}

module.exports.getAnn = function(req, res, next){
	//console.log();
		/*Ann.paginate({},parseInt(req.body["indexOfAnn"]),2,function(error, paginatedResults) {
			if (error) {
				console.error(error);
			} else {
				console.log('Pages:');
				console.log(paginatedResults);
			}
		});*/
	console.log(req.url);
	var q = url.parse(req.url, true);
	console.log(q.query.category);
	var txt = q.query.category;
	if(req.url == "/")
	{
		txt = "All";
	}
	
	Ann.find((err, anns) => {  
		if (err) {
			// Note that this error doesn't mean nothing was found,
			// it means the database had an error while searching, hence the 500 status
			res.status(500).send(err)
		} else {
			// send the list of all announcements
			//console.log(anns[0]);
			if(txt != 'All')
			{
				for(i = anns.length-1; i>=0;i--)
				{
					if(anns[i]["category"] != txt)
						anns.splice(i,1);
				}
			}
			returnJson(res,200,{"stat": anns});
		}
	});
}

module.exports.login = function(req, res, next) {
  res.render('login', { title: 'Express' });
}

module.exports.phone_category = function(req, res, next) {
  res.render('phone_category', { title: 'Express' });
}

module.exports.phone_search = function(req, res, next) {
  res.render('phone_search', { title: 'Express' });
}

module.exports.register = function(req, res, next) {
  res.render('register', { title: 'Express' });
}

module.exports.user = function(req, res, next) {
  res.render('user', { title: 'Express' });
}

module.exports.db = function(req, res, next) {
  res.render('db', { title: 'Express' });
}

module.exports.checkLogin = function(req, res, next) {
    var data = req.body;
    var username = data.email;
    var password = data.password;
    
    if (username == "example@sth.com" && password == "12345678") {
        res.render('user');
	} else {
        res.render('login', {error: true, username: username, password: password});
	}
 }

var returnJson = function(res, status, content) {
  res.status(status);
  res.json(content);
};

//module.exports.addMockups = function(req, res){
function addMockups(req, res){
	var userDB1 = new User({
		_id: new mongoose.Types.ObjectId(),
		nickname: "Tosiek",
		email: "example@sth.com",
		note: "I am fabulous",
		imgPath: "./images/default.jpg"
	});
		var userDB2 = new User({
		_id: new mongoose.Types.ObjectId(),
		nickname: "Madzia",
		email: "sthelse@sth.com",
		note: "For the Horde",
		imgPath: "./images/default.jpg"
	});
	
	var annDB1 = new Ann({
		_id: new mongoose.Types.ObjectId(),
		title: "Amazing title",
		category: "Buy",
		loc: "Ljubljana",
		content: "I want to buy a car",
		imgPath: "./images/megasushi.jpg",
		author: userDB1
	});
	var annDB2 = new Ann({
		_id: new mongoose.Types.ObjectId(),
		title: "Title kappa",
		category: "Buy",
		loc: "Ljubljana",
		content: "I would like to know what I am doing.",
		imgPath: "./images/default.jpg",
		author: userDB2
	});
	var annDB3 = new Ann({
		_id: new mongoose.Types.ObjectId(),
		title: "Lenovo y580",
		category: "Sell",
		loc: "Ljubljana",
		content: "I want to sell laptop.",
		imgPath: "./images/default.jpg",
		author: userDB1
	});
	
	var catDB1 = new Category({
		title: "Buy",
		id: 1
	});
	var catDB2 = new Category({
		title: "Sell",
		id: 2
	});

	
	userDB1.save(function(err) {
        var saved = (err ? false : true);
        //returnJson(res, 200, {"saved": saved});
    });
    userDB2.save(function(err) {
        var saved = (err ? false : true);
        //returnJson(res, 200, {"saved": saved});
    });
	annDB1.save(function(err) {
        var saved = (err ? false : true);
        //returnJson(res, 200, {"saved": saved});
    });
	annDB2.save(function(err) {
        var saved = (err ? false : true);
        //returnJson(res, 200, {"saved": saved});
    });
	annDB3.save(function(err) {
        var saved = (err ? false : true);
        //returnJson(res, 200, {"saved": saved});
    });
	catDB1.save(function(err) {
        var saved = (err ? false : true);
        //returnJson(res, 200, {"saved": saved});
    });
	catDB2.save(function(err) {
        var saved = (err ? false : true);
        //returnJson(res, 200, {"saved": saved});
    });
}

module.exports.addNewAnn = function(req, res){
	var user = new User({
		_id: new mongoose.Types.ObjectId(),
		nickname: "Current user",
		email: "example@sth.com",
		note: "I am fabulous and handsome",
		imgPath: "./images/default.jpg"
	});
	
	var ann = new Ann({
		_id: new mongoose.Types.ObjectId(),
		title: req.body["title"],
		category: req.body["category"],
		loc: req.body["loc"],
		content: req.body["content"],
		imgPath: "./images/default.jpg",
		author: user
	});
	
	ann.save(function(err) {
        var saved = (err ? false : true);
        //returnJson(res, 200, {"saved": saved});
    });
	console.log("Added new announcement.");
	res.render('announcement', { title: 'Express' });
}

function deleteOne(item)
{
	item.remove();
}

function removeAllObjects(error,objects)
{
		if (error) {
			// Note that this error doesn't mean nothing was found,
			// it means the database had an error while searching, hence the 500 status
			res.status(500).send(error)
		} else {
			// send the list of all people
			objects.forEach(deleteOne);
		}
}


function deleteAll(req, res)
{
	User.find((err,users) => removeAllObjects(err,users));
	Ann.find((err,anns) => removeAllObjects(err,anns));
	Category.find((err,cats) => removeAllObjects(err,cats));
}

module.exports.asdf = function(req, res){
	//console.log(req.url);
	var q = url.parse(req.url, true);
	console.log(q.query.dosth);
	if(q.query.dosth == 'createdb')
		addMockups(req, res);
	else if(q.query.dosth == 'deletedb')
		deleteAll(req,res);
	res.render('db', { title: 'Express' });
}