var express = require('express');
var router = express.Router();
var ctrlMain = require('../controllers/main');

/* GET home page. */
router.get('/', ctrlMain.crud);
router.post('/', ctrlMain.addNewAnn);

module.exports = router;
