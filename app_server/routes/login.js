var express = require('express');
var router = express.Router();
var ctrlMain = require('../controllers/main');

router.get('/', ctrlMain.login);
router.post('/', ctrlMain.checkLogin);

module.exports = router;
