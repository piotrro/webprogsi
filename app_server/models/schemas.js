var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var userSchema = new mongoose.Schema({
    nickname: String,
	email: String,
	note: String,
	imgPath: String
});

var annSchema = new mongoose.Schema({
	title: String,
	category: String,
	loc: String,
	content: String,
	imgPath: String,
	author: userSchema
});
annSchema.plugin(mongoosePaginate);

var categorySchema = new mongoose.Schema({
    title: String,
	id: Number
});

var adminSchema = new mongoose.Schema({
	nickname: String,
	email: String
});

mongoose.model('announcement', annSchema, 'announcements');
mongoose.model('User', userSchema, 'Users');
mongoose.model('category', categorySchema, 'categories');
mongoose.model('admin', adminSchema, 'admins');