var express = require('express');
var router = express.Router();
require('./schemas');
var mongoose = require('mongoose');
var ctrlMain = require('../controllers/main');

var dbURI = 'mongodb://localhost/webprograming';
mongoose.connect(dbURI, { useMongoClient: true });

mongoose.connection.on('connected', function() {
  console.log('Mongoose is connected to ' + dbURI);
});
mongoose.connection.on('error', function(err) {
  console.log('Mongoose error : ' + err);
});
mongoose.connection.on('disconnected', function() {
  console.log('Mongoose was closed.');
});

var pravilnaUstavitev = function(sporocilo, povratniKlic) {
  mongoose.connection.close(function() {
    console.log('Mongoose je zaprl povezavo preko ' + sporocilo);
    povratniKlic();
  });
};

// When restarting nodemon
process.once('SIGUSR2', function() {
  pravilnaUstavitev('nodemon restart', function() {
    process.kill(process.pid, 'SIGUSR2');
  });
});

// When exiting app
process.on('SIGINT', function() {
  pravilnaUstavitev('exit from the app', function() {
    process.exit(0);
  });
});

// Upon exiting app on Heroku
process.on('SIGTERM', function() {
  pravilnaUstavitev('exit from app on Heroku', function() {
    process.exit(0);
  });
});
