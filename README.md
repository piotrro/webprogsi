# README #

## Task 3 ##

### Running application locally ###

* To run my app locally you have to clone the repo, and install express package (npm install express).
* (if mongoDB is already on on your pc, pass this step) Run mongoDB by script in mongodb folder (cd mongodb ./mongod)
* Run ap by node.js (node ./bin/www)

### Heroku link ###

* [My app on Heroku](https://announcement-sys.herokuapp.com/)

## Task 2 ##

### Diferences between browsers ###

* On Chrome and Opera images are scaled with quality loss, not like on Firefox.
* On Firefox fonts look slightly different.

## Task 1 ##

### What my application do? ###

* The application is a website, where anyone can make his account and announcements. You can announce that you have something to buy or sell, that you are looking for job/emplyee, that you are looking for ride to your favourite city or... whatever you want to.

### Wireframes ###

* [announcement.png](https://bytebucket.org/piotrro/webprogsi/raw/5689f25f90f948bbb5a0bbec30b47ca02afa3a8a/docs/announcement.png) - shows how the announcement looks like (one choosen announcement). White-gray button "Edit/Delete" shows only to owner of announcement.
* [crud.png](https://bytebucket.org/piotrro/webprogsi/raw/5689f25f90f948bbb5a0bbec30b47ca02afa3a8a/docs/crud.png) - shows how user can add new announcement. Same tamplate, but with changed names of buttons and title, will be used to page, where user can edit or delete his announcement.
* [login.png](https://bytebucket.org/piotrro/webprogsi/raw/5689f25f90f948bbb5a0bbec30b47ca02afa3a8a/docs/login.png) - page allowing the user to log in.
* [main.png](https://bytebucket.org/piotrro/webprogsi/raw/5689f25f90f948bbb5a0bbec30b47ca02afa3a8a/docs/main.png) - index of the webpage, main template. Shows how announcements are listed, how to choose category, look for announcements or add new. White-gray button "Edit" shows only to owner of announcement.
* [register.png](https://bytebucket.org/piotrro/webprogsi/raw/5689f25f90f948bbb5a0bbec30b47ca02afa3a8a/docs/register.png) - registration form for creating new account.
* [phone\_announcement.png](https://bytebucket.org/piotrro/webprogsi/raw/5689f25f90f948bbb5a0bbec30b47ca02afa3a8a/docs/phone\_announcement.png) - equivalent of announcement.png, but for phone screens. White-gray button "Edit/Delete" shows only to owner of announcement.
* [phone\_category.png](https://bytebucket.org/piotrro/webprogsi/raw/5689f25f90f948bbb5a0bbec30b47ca02afa3a8a/docs/phone\_category.png) - menu where user is able to choose a category of announcements. Version for phone screens.
* [phone\_crud.png](https://bytebucket.org/piotrro/webprogsi/raw/5689f25f90f948bbb5a0bbec30b47ca02afa3a8a/docs/phone\_crud.png) - equivalent of crud.png, but for phone screens.
* [phone\_login.png](https://bytebucket.org/piotrro/webprogsi/raw/5689f25f90f948bbb5a0bbec30b47ca02afa3a8a/docs/phone\_login.png) - equivalent of login.png, but for phone screens.
* [phone\_main.png](https://bytebucket.org/piotrro/webprogsi/raw/5689f25f90f948bbb5a0bbec30b47ca02afa3a8a/docs/phone\_main.png) - equivalent of main.png, but for phone screens. Instead of "View" button (see main.png) the user can simply click on announcement box.
* [phone\_register.png](https://bytebucket.org/piotrro/webprogsi/raw/5689f25f90f948bbb5a0bbec30b47ca02afa3a8a/docs/phone\_register.png) - equivalent of register.png, but for phone screens.
* [phone\_search.png](https://bytebucket.org/piotrro/webprogsi/raw/5689f25f90f948bbb5a0bbec30b47ca02afa3a8a/docs/phone\_search.png) - shows page where the user can search for announcements. Version for phone screens.
* [flow.png](https://bytebucket.org/piotrro/webprogsi/raw/5689f25f90f948bbb5a0bbec30b47ca02afa3a8a/docs/flow.png) - shows how the user can move from screen to another screen.


### User Stories ###

* As a User, I would like to be able to create account, co my data will be stored, and I would not have to put my info in every announcement.
* As a User, I would like the application be able to show me my annoucements, so I won't have to look for them.
* As a User, I would like to be able to read and search for annoucements.
* As a User, I would like to be able to edit my announcements in case I change my mind or make a mistake.
* As a User, I would like to be able to choose a category of announcements, to make my searching faster.

### Who do I talk to? ###

* Repo owner - Piotr G�ralewski, Erasmus student from Poland