var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

require('./app_server/models/db');

var index = require('./app_server/routes/index');
var db = require('./app_server/routes/db');
var crud = require('./app_server/routes/crud');
var announcement = require('./app_server/routes/announcement');
var login = require('./app_server/routes/login');
var phone_category = require('./app_server/routes/phone_category');
var phone_search = require('./app_server/routes/phone_search');
var register = require('./app_server/routes/register');
var user = require('./app_server/routes/user');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'app_server', 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/index', index);
app.use('/db', db);
app.use('/crud', crud);
app.use('/announcement', announcement);
app.use('/login', login);
app.use('/phone_category', phone_category);
app.use('/phone_search', phone_search);
app.use('/register', register);
app.use('/user', user);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
