var express = require('express');
var router = express.Router();
var ctrlMain = require('../public/controllers/main');

/* GET home page. */
router.get('/', ctrlMain.register);

module.exports = router;
